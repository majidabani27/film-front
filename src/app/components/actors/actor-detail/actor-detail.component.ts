import { Component, OnInit } from '@angular/core';
import { Actor } from '../../../models/actor.model';
import { Film } from '../../../models/film.model';
import { ActivatedRoute, Router } from '@angular/router';
import { ActorService } from '../../../services/actor.service';
import { AuthService } from '../../../services/auth.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-actor-detail',
  templateUrl: './actor-detail.component.html',
  styleUrls: ['./actor-detail.component.css']
})
export class ActorDetailComponent implements OnInit {

  public firstName: String;
  public lastName: String;
  public image: String;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private actorService: ActorService,
    private authService: AuthService,
    private snackBar: MatSnackBar
  ) { }

  protected actor: Actor;
  protected films: Film[];

  // get the actor id from route params put it in a const id (id is necessary) and load actor then load the actors films in the films array.
  ngOnInit() {
    console.log('ActorDetailComponent on init');
    this.route.paramMap.subscribe(params => {
      const actorId: string = params.get('id');
      this.actorService.getById(actorId).subscribe(captActor => {
        this.actor = captActor;
        this.actorService.getFilms(this.actor).subscribe(data => {
          console.log(data);
          this.films = data;

          this.firstName = captActor.firstName;
          this.lastName = captActor.lastName;
          this.image = captActor.image;
        });
      });
    });
  }
  // delete the actor using the actirservice.delete method open a snackbar with duration 3000 to inform that the actor has been deleted and then navigate to ''.
  delete() {
    this.actorService.delete(this.actor).subscribe(response => {
      this.snackBar.open('The actor' + this.actor.firstName + ' ' + this.actor.lastName + ' has been deleted', null, {
        duration: 3000
      });
      this.router.navigate(['']);
    });
  }
}
