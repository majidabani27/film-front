import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FilmService } from '../../../services/film.service';
import { Film } from '../../../models/film.model';
import { Observable } from 'rxjs/Observable';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { Director } from '../../../models/director.model';
import { Actor } from '../../../models/actor.model';
import { ModelHelper } from '../../../shared/model.helper';
import { AuthService } from '../../../services/auth.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-film-detail',
  templateUrl: './film-detail.component.html',
  styleUrls: ['./film-detail.component.css']
})
export class FilmDetailComponent implements OnInit {

  public image: String;
  public title: String;


  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private filmService: FilmService,
    private authService: AuthService,
    private snackBar: MatSnackBar
  ) {
  }

  protected film: Film;

  protected actors: Actor[];

  protected directors: Director[];
  // get the film id from route params put it in a const id (id is necessary) and load film then load the film's actors/directors
  // in the actors/directors array.
  ngOnInit() {
    console.log('FilmDetailComponent on init');
    this.route.paramMap.subscribe(params => {
      const filmId: string = params.get('id');
      this.filmService.getById(filmId).subscribe(captureFilm => {
        this.film = captureFilm;
        this.title = captureFilm.title;
        this.image = captureFilm.image;
        this.filmService.getActors(this.film).subscribe(data => {
          this.actors = data;
        });
        this.filmService.getDirectors(this.film).subscribe(data => {
          this.directors = data;
        });

      });
    });
  }
  // delete the actor using the filmservice method open a snackbar with duration 3000 to inform that the film has been deleted and then navigate to ''.
  delete() {
    this.filmService.delete(this.film).subscribe(response => {
      this.snackBar.open('film' + this.film.title + '\' has been deleted', null, {
        duration: 3000
      });
      this.router.navigate(['']);
    });

  }
}
