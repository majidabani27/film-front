import { Component, OnInit } from '@angular/core';
import { Director } from '../../../models/director.model';
import { Film } from '../../../models/film.model';
import { ActivatedRoute, Router } from '@angular/router';
import { DirectorService } from '../../../services/director.service';
import { AuthService } from '../../../services/auth.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-director-detail',
  templateUrl: './director-detail.component.html',
  styleUrls: ['./director-detail.component.css']
})
export class DirectorDetailComponent implements OnInit {

  public firstName: String;
  public lastName: String;
  public image: String;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private directorService: DirectorService,
    private authService: AuthService,
    private snackBar: MatSnackBar
  ) { }

  protected director: Director;

  protected films: Film[];

  // get the director id from route params and load director and his films
  ngOnInit() {
    console.log('DirectorDetailComponent on init');
    this.route.paramMap.subscribe(params => {
      const directorId: string = params.get('id');
      this.directorService.getById(directorId).subscribe(captureDirector => {
        this.director = captureDirector;
        this.directorService.getFilms(this.director).subscribe(data => {
          console.log(data);
          this.films = data;

          this.firstName = captureDirector.firstName;
          this.lastName = captureDirector.lastName;
          this.image = captureDirector.image;
        });
      });

    });
  }

  delete() {
    this.directorService.delete(this.director).subscribe(response => {
      this.snackBar.open('Director ' + this.director.firstName + ' ' + this.director.lastName + '\'hase been deleted', null, {
        duration: 3000
      });
      this.router.navigate(['']);
    });
  }
}
